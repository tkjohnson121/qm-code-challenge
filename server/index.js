const { port: PORT } = require('./config');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const errorHandler = require('./handlers/error');

// Route/Handlers
const predicateRoutes = require('./routes/predicates');

// Init App
const app = express();

// Init CORS and bodyParser
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// All Routes
app.use('/api/predicate', predicateRoutes); // Predicate Builder

// 404
app.use((req, res, next) => {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// Error Handler
app.use(errorHandler);

app.listen(PORT, () => {
  console.log(`Server is starting on port ${PORT}`);
});
