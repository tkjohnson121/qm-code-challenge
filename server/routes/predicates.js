const express = require('express');
const router = express.Router({ mergeParams: true });

const { createPredicate } = require('../handlers/predicate');

router.route('/new').post(createPredicate);

module.exports = router;
