exports.createPredicate = async function(req, res, next) {
  let predicateStatements = [];
  try {
    req.body.map(function(predicate) {
      const { selector, operator, input, secondaryInput } = predicate;
      let predicateString = '';

      if (selector.type === 'string') {
        switch (operator) {
          case 'equals': {
            predicateString = `SELECT * FROM session WHERE ${selector.value} = ${input}`;
            break;
          }

          case 'contains': {
            predicateString = `SELECT * FROM session WHERE ${selector.value} LIKE '%${input}%'`;
            break;
          }

          case 'starts_with': {
            predicateString = `SELECT * FROM session WHERE ${selector.value} LIKE '${input}%'`;
            break;
          }

          case 'in_list': {
            predicateString = `SELECT * FROM session WHERE ${selector.value} IN (${input})`;
            break;
          }

          default: {
            break;
          }
        }
      } else if (selector.type === 'number') {
        switch (operator) {
          case 'equals': {
            predicateString = `SELECT * FROM session WHERE ${selector.value} = ${input}`;
            break;
          }

          case 'greater_than': {
            predicateString = `SELECT * FROM session WHERE ${selector.value} > ${input}%'`;
            break;
          }

          case 'less_than': {
            predicateString = `SELECT * FROM session WHERE ${selector.value} < ${input}`;
            break;
          }

          case 'range': {
            predicateString = `SELECT * FROM session WHERE ${selector.value} BETWEEN ${input} AND ${secondaryInput}`;
            break;
          }

          case 'in_list': {
            predicateString = `SELECT * FROM session WHERE ${selector.value} IN (${input})`;
            break;
          }

          default: {
            break;
          }
        }
      }

      predicateStatements.push(predicateString);
    });

    //return necessary data
    console.log(predicateStatements.join(' UNION '));
    return res.status(200).send(predicateStatements.join(' UNION '));
  } catch (err) {
    next(err);
  }
};
