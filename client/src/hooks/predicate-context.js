import React from 'react'
import axios from 'axios'
import PropTypes from 'prop-types'
const PredicateStateContext = React.createContext()
const PredicateDispatchContext = React.createContext()

function predicateReducer(state, action) {
  const { type, payload } = action

  switch (action.type) {
    case 'add-predicate': {
      return [
        ...state,
        {
          selector: { type: '', value: '', text: '' },
          operator: '',
          input: '',
          secondaryInput: '',
          errors: { selector: '', operator: '', input: '', secondaryInput: '' },
        },
      ]
    }

    case 'remove-predicate': {
      return state.filter((predicate, index) => index !== payload.index)
    }

    case 'update-predicate': {
      return state.map((predicate, index) => {
        if (index !== payload.index) return predicate

        return { ...predicate, [payload.type]: payload.value }
      })
    }

    default: {
      throw new Error(`Unhandled action type: ${type}`)
    }
  }
}

const initialState = [
  {
    selector: { type: '', value: '', text: '' },
    operator: '',
    input: '',
    secondaryInput: '',
    errors: { selector: '', operator: '', input: '', secondaryInput: '' },
  },
]

function PredicateProvider({ children }) {
  const [state, dispatch] = React.useReducer(predicateReducer, initialState)

  return (
    <PredicateStateContext.Provider value={state}>
      <PredicateDispatchContext.Provider value={dispatch}>
        {children}
      </PredicateDispatchContext.Provider>
    </PredicateStateContext.Provider>
  )
}

PredicateProvider.propTypes = {
  children: PropTypes.object,
}

function usePredicateState() {
  const context = React.useContext(PredicateStateContext)

  if (context === undefined) {
    throw new Error('usePredicateState must be used within a PredicateProvider')
  }

  return context
}

function usePredicateDispatch() {
  const context = React.useContext(PredicateDispatchContext)

  if (context === undefined) {
    throw new Error(
      'usePredicateDispatch must be used within a PredicateProvider',
    )
  }

  return context
}

async function submitPredicate(predicates) {
  try {
    return axios.post('http://localhost:8081/api/predicate/new/', predicates)
  } catch (error) {
    throw new Error(error)
  }
}

export {
  PredicateProvider,
  usePredicateDispatch,
  usePredicateState,
  submitPredicate,
}
