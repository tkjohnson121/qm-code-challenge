import React from 'react'
import Layout from '../../components/layout'
import PredicateForm from '../predicate-form'
import { PredicateProvider } from '../../hooks/predicate-context'
import { PrimaryButton } from '../../styles/button'

function App() {
  return (
    <Layout>
      <header
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          padding: '0.5rem 1rem',
        }}
      >
        <h1
          style={{
            marginBottom: '0',
            opacity: '60%',
            fontSize: '20px',
            textTransform: 'uppercase',
          }}
        >
          Search For Sessions
        </h1>
        <PrimaryButton>Today</PrimaryButton>
      </header>
      <hr />
      <main>
        <PredicateProvider>
          <PredicateForm />
        </PredicateProvider>
      </main>
    </Layout>
  )
}

export default App
