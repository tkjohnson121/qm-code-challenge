import React from 'react'
import {
  usePredicateState,
  usePredicateDispatch,
  submitPredicate,
} from '../../hooks/predicate-context'
import Predicate from '../../components/predicate'
import { Form, FormActions } from '../../styles/form-styled'
import { PrimaryButton } from '../../styles/button'

function PredicateForm() {
  const predicateState = usePredicateState()
  const predicateDispatch = usePredicateDispatch()

  const validate = (state = predicateState, dispatch = predicateDispatch) => {
    let valid = true

    state.map(async (obj, index) => {
      let newErrors = {}

      if (obj.selector.type === '') {
        valid = false
        newErrors.selector = 'Selector cannot be empty'
      } else {
        newErrors.selector = ''
      }

      if (obj.operator === '') {
        valid = false
        newErrors.operator = 'Operator cannot be empty'
      } else {
        newErrors.operator = ''
      }

      if (obj.input === '') {
        valid = false
        newErrors.input = 'Input cannot be empty'
      } else {
        newErrors.input = ''
      }

      if (obj.operator === 'range' && obj.secondaryInput === '') {
        valid = false
        newErrors.secondaryInput = 'Secondary Input cannot be empty'
      } else {
        newErrors.secondaryInput = ''
      }

      await dispatch({
        type: 'update-predicate',
        payload: {
          index,
          type: 'errors',
          value: { ...newErrors },
        },
      })
    })

    return valid
  }

  const onFormSubmit = async (e) => {
    e.preventDefault()

    let valid = validate()

    if (!valid) return

    console.log(await submitPredicate(predicateState))
  }

  return (
    <Form onSubmit={onFormSubmit}>
      {predicateState.map((predicate, index) => (
        <Predicate
          key={index}
          index={index}
          predicate={predicate}
          dispatch={predicateDispatch}
          validate={validate}
        />
      ))}
      <PrimaryButton
        type="button"
        onClick={() => predicateDispatch({ type: 'add-predicate' })}
      >
        AND
      </PrimaryButton>

      <FormActions>
        <PrimaryButton>Search</PrimaryButton>
      </FormActions>
    </Form>
  )
}

export default PredicateForm
