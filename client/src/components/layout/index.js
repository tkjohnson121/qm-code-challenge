import React from 'react'
import PropTypes from 'prop-types'
import { ThemeProvider } from 'emotion-theming'
import theme from '../../styles/theme'
import './layout.css'

function Layout({ children, ...rest }) {
  return (
    <ThemeProvider theme={theme} {...rest}>
      {children}
    </ThemeProvider>
  )
}

Layout.propTypes = {
  children: PropTypes.array,
}

export default Layout
