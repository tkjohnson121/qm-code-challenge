/* eslint-disable no-fallthrough */
// resetFields function relies on fallthrough, only remove if refactored
import React from 'react'
import PropTypes from 'prop-types'
import styled from '@emotion/styled'
import Selectors from './selectors'
import Operators from './operators'
import InputSection from './input-section'
import { Fieldset, Errors } from '../../styles/form-styled'
import { SecondaryButton } from '../../styles/button'

const PredicateErrors = styled.div`
  flex-basis: 100%;
`

function Predicate({ index, predicate, dispatch, validate, ...rest }) {
  const { selector, operator, input, secondaryInput, errors } = predicate
  const {
    selector: selectorError,
    operator: operatorError,
    input: inputError,
    secondaryInput: secondaryInputError,
  } = errors

  const onUpdate = async (type, value, index) => {
    await dispatch({
      type: 'update-predicate',
      payload: { index, type, value },
    })
    await resetFields(type, index)

    if (selectorError || operatorError || inputError || secondaryInputError) {
      console.log('validating')
      validate()
    }
  }

  const onDelete = (index) => {
    dispatch({ type: 'remove-predicate', payload: { index } })
  }

  /**
   * Resets all following fields when a change is made.
   * Each case resets the field that comes after
   * DO NOT remove eslint-disable-next-line
   */
  const resetFields = async (type, index) => {
    switch (type) {
      case 'selector': {
        await dispatch({
          type: 'update-predicate',
          payload: { index, type: 'operator', value: '' },
        })
      }
      case 'operator': {
        await dispatch({
          type: 'update-predicate',
          payload: { index, type: 'input', value: '' },
        })
      }
      case 'input': {
        await dispatch({
          type: 'update-predicate',
          payload: { index, type: 'secondaryInput', value: '' },
        })

        return
      }
      case 'secondaryInput': {
        return
      }
      default:
        throw new Error(`Unhandled reset type: ${type}`)
    }
  }

  return (
    <Fieldset {...rest}>
      <SecondaryButton type="button" onClick={() => onDelete(index)}>
        -
      </SecondaryButton>
      <Selectors
        selector={selector}
        onUpdate={(type, value) => onUpdate(type, value, index)}
        selectorError={selectorError}
      />

      <Operators
        selector={selector}
        operator={operator}
        onUpdate={(type, value) => onUpdate(type, value, index)}
        operatorError={operatorError}
      />

      <InputSection
        operator={operator}
        selector={selector}
        input={input}
        secondaryInput={secondaryInput}
        onUpdate={(type, value) => onUpdate(type, value, index)}
        inputError={inputError}
        secondaryInputError={secondaryInputError}
      />

      <PredicateErrors>
        {Object.values(errors).map((error) => {
          if (!error) return null
          return <Errors key={error}>{error}</Errors>
        })}
      </PredicateErrors>
    </Fieldset>
  )
}

Predicate.propTypes = {
  predicate: PropTypes.object,
  index: PropTypes.number,
  dispatch: PropTypes.func,
  validate: PropTypes.func,
}

export default Predicate
