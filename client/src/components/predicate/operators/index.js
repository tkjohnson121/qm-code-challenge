import React from 'react'
import PropTypes from 'prop-types'
import { Select } from '../../../styles/form-styled'

const stringOptions = [
  {
    value: 'starts_with',
    text: 'starts with',
  },
  {
    value: 'equals',
    text: 'equals',
  },
  {
    value: 'contains',
    text: 'contains',
  },
  {
    value: 'in_list',
    text: 'in list',
  },
]

const integerOptions = [
  {
    value: 'equals',
    text: 'equals',
  },
  {
    value: 'range',
    text: 'range',
  },
  {
    value: 'greater_than',
    text: 'greater-than',
  },
  {
    value: 'less_than',
    text: 'less-than',
  },
  {
    value: 'in_list',
    text: 'in-list',
  },
]

function Operators({ selector, operator, onUpdate }) {
  const changeOperator = (operatorValue) => {
    // Reset operator
    if (operatorValue === 'default') {
      onUpdate('operator', '')

      return
    }

    onUpdate('operator', operatorValue)
    return
  }

  return (
    <>
      {selector.value === 'between' && <span>is</span>}
      <Select onChange={(e) => changeOperator(e.target.value)} value={operator}>
        {selector.type === 'string'
          ? stringOptions.map((option) => (
              <option value={option.value} key={option.text}>
                {option.text}
              </option>
            ))
          : integerOptions.map((option) => (
              <option value={option.value} key={option.text}>
                {option.text}
              </option>
            ))}
      </Select>
    </>
  )
}

Operators.propTypes = {
  selector: PropTypes.object,
  operator: PropTypes.string,
  onUpdate: PropTypes.func,
}

export default Operators
