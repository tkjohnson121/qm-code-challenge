import React from 'react'
import PropTypes from 'prop-types'
import { TextInput } from '../../../styles/form-styled'

function InputSection({ operator, selector, input, secondaryInput, onUpdate }) {
  if (selector && operator !== 'range') {
    return (
      <TextInput
        type={
          selector.type === 'string'
            ? 'text'
            : !selector.type
            ? 'text'
            : 'number'
        }
        onChange={(e) => onUpdate('input', e.target.value)}
        value={input}
      />
    )
  } else if (selector && operator === 'range') {
    return (
      <>
        <TextInput
          type="number"
          onChange={(e) => onUpdate('input', e.target.value)}
          value={input}
        />
        <span>and</span>
        <TextInput
          type="number"
          onChange={(e) => onUpdate('secondaryInput', e.target.value)}
          value={secondaryInput}
          key={'secondary-input'}
        />
      </>
    )
  } else return null
}

InputSection.propTypes = {
  operator: PropTypes.string,
  selector: PropTypes.object,
  input: PropTypes.string,
  secondaryInput: PropTypes.string,
  onUpdate: PropTypes.func,
}

export default InputSection
