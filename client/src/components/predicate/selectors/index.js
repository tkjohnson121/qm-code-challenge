import React from 'react'
import PropTypes from 'prop-types'
import { Select } from '../../../styles/form-styled'

const selectors = [
  {
    value: 'user_email',
    text: 'User Email',
    type: 'string',
  },
  {
    value: 'user_first_name',
    text: 'First Name',
    type: 'string',
  },
  {
    value: 'user_last_name',
    text: 'Last Name',
    type: 'string',
  },
  {
    value: 'screen_width',
    text: 'Screen Width',
    type: 'number',
  },
  {
    value: 'screen_height',
    text: 'Screen Height',
    type: 'number',
  },
  {
    value: 'visits',
    text: '# of Visits',
    type: 'number',
  },
  {
    value: 'page_response',
    text: 'Page Response Time (ms)',
    type: 'number',
  },
  {
    value: 'domain',
    text: 'Domain',
    type: 'string',
  },
  {
    value: 'path',
    text: 'Page Path',
    type: 'string',
  },
]

function Selectors({ selector, onUpdate, ...rest }) {
  const changeSelector = (selectorValue) => {
    // Reset selector
    if (selectorValue === 'default') {
      onUpdate('selector', { type: '', value: '', text: '' })
      return
    }

    // Filters out the correct selector based on the value
    const newSelector = selectors.filter(
      (selector) => selector.value === selectorValue,
    )[0]

    onUpdate('selector', newSelector)
    return
  }

  return (
    <Select
      onChange={(e) => changeSelector(e.target.value)}
      value={selector.value}
      {...rest}
    >
      <option value="default" defaultValue>
        Event Triggered
      </option>
      {selectors.map((selector) => (
        <option value={selector.value} key={selector.text}>
          {selector.text}
        </option>
      ))}
    </Select>
  )
}

Selectors.propTypes = {
  changeSelector: PropTypes.func,
  selector: PropTypes.object,
  onUpdate: PropTypes.func,
}

export default Selectors
