import styled from '@emotion/styled'

const Form = styled.form`
  margin: 1rem auto;
  position: relative;
  min-height: 30vh;
  padding: 0 1rem;
`

const FormActions = styled.div`
  position: absolute;
  bottom: 0;
  right: 0;
`

const Fieldset = styled.fieldset`
  position: relative;
  padding: 0.5rem 1rem;
  margin: 1rem auto;
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;

  & > * {
    margin-bottom: 1rem;
  }

  @media screen and (min-width: ${(p) => p.theme.breakpoints[2]}) {
    border: none;
    flex-direction: row;

    & > * {
      margin-bottom: 0;
    }
  }
`

const Select = styled.select`
  padding: 0.6em 1.4em 0.5em 0.8em;
  margin-left: 0.5rem;
  margin-right: 0.5rem;
  border: 2px solid ${(p) => p.theme.palette.gray[5]};
  border-radius: 0.5rem;
  appearance: none;
  background-image: url('data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22292.4%22%20height%3D%22292.4%22%3E%3Cpath%20fill%3D%22%23007CB2%22%20d%3D%22M287%2069.4a17.6%2017.6%200%200%200-13-5.4H18.4c-5%200-9.3%201.8-12.9%205.4A17.6%2017.6%200%200%200%200%2082.2c0%205%201.8%209.3%205.4%2012.9l128%20127.9c3.6%203.6%207.8%205.4%2012.8%205.4s9.2-1.8%2012.8-5.4L287%2095c3.5-3.5%205.4-7.8%205.4-12.8%200-5-1.9-9.2-5.5-12.8z%22%2F%3E%3C%2Fsvg%3E'),
    linear-gradient(to bottom, #ffffff 0%, #e5e5e5 100%);
  background-repeat: no-repeat, repeat;
  background-position: right 0.7em top 50%, 0 0;
  background-size: 0.65em auto, 100%;

  &:hover {
    border-color: ${(p) => p.theme.palette.gray[8]};
  }

  &:focus {
    border-color: ${(p) => p.theme.palette.gray[8]};
    box-shadow: 0 0 1px 3px rgba(59, 153, 252, 0.7);
  }
`

const TextInput = styled.input`
  padding: 0.3rem 0.7rem;
`

const Errors = styled.span`
  padding: 0.5rem 1rem;
  color: ${(p) => p.theme.palette.red[7]};
`

export { Form, FormActions, Fieldset, Select, TextInput, Errors }
