/**
 * Basic theme
 */

const theme = {
  palette: {
    primary: {
      contrast: '#FAFAFF',
      main: '#138ec5',
    },

    secondary: {
      contrast: '#aaa',
      main: '#242424',
      dark: '#181818',
    },

    gray: [
      '#fafaff',
      '#efedec',
      '#e4e0df',
      '#d7d2d0',
      '#c9c2c0',
      '#bab1ae',
      '#a99d9a',
      '#948682',
      '#7a6863',
      '#493d39',
    ],

    red: [
      '#fdf8f7',
      '#faeae5',
      '#f6dbd3',
      '#f1cbbe',
      '#ecb8a7',
      '#e7a38c',
      '#e08a6d',
      '#d76944',
      '#c93503',
      '#781f01',
    ],

    orange: [
      '#fcf9f1',
      '#f6edd4',
      '#efe0b4',
      '#e7d291',
      '#dfc169',
      '#d5af3b',
      '#ca9805',
      '#ad8202',
      '#896701',
      '#503c01',
    ],

    yellow: [
      '#f7fbed',
      '#e7f3c5',
      '#d5e99a',
      '#c1df68',
      '#a9d22e',
      '#92c302',
      '#83ae02',
      '#709501',
      '#587601',
      '#344501',
    ],

    lime: [
      '#f3fcf0',
      '#daf5d1',
      '#beeeae',
      '#9ee585',
      '#76db55',
      '#44cd15',
      '#30b802',
      '#299e02',
      '#207d01',
      '#134a01',
    ],

    green: [
      '#f1fcf4',
      '#d4f6dd',
      '#b3efc2',
      '#8ce6a3',
      '#5cdc7c',
      '#1bce48',
      '#02b930',
      '#029f29',
      '#017e21',
      '#014a13',
    ],

    teal: [
      '#f0fcf9',
      '#d1f5ec',
      '#adeedd',
      '#83e5cc',
      '#4fdab7',
      '#0acb9a',
      '#02b588',
      '#029c75',
      '#017b5d',
      '#014937',
    ],

    cyan: [
      '#f4fbfd',
      '#ddf1f8',
      '#c4e7f2',
      '#a7dbec',
      '#88cee6',
      '#63bfde',
      '#35acd4',
      '#0293c4',
      '#02759b',
      '#01455c',
    ],

    blue: [
      '#f8f9fd',
      '#e9edfa',
      '#d9e0f7',
      '#c7d2f3',
      '#b4c3ef',
      '#9fb2ea',
      '#869ee5',
      '#6785df',
      '#3e64d6',
      '#022daf',
    ],

    indigo: [
      '#faf9fe',
      '#efebfb',
      '#e4ddf8',
      '#d7cef4',
      '#cabcf1',
      '#baa9ed',
      '#a893e8',
      '#9278e2',
      '#7553da',
      '#3402c9',
    ],

    violet: [
      '#fcf8fe',
      '#f6e9fa',
      '#f0d9f7',
      '#e8c8f3',
      '#e0b5ef',
      '#d79eea',
      '#cc84e5',
      '#be62dd',
      '#a92dd2',
      '#6a018d',
    ],

    fuschia: [
      '#fdf8fc',
      '#fae9f6',
      '#f7d8ef',
      '#f3c6e7',
      '#eeb1df',
      '#e999d5',
      '#e37cc9',
      '#db55b9',
      '#ca0799',
      '#7b015c',
    ],

    pink: [
      '#fef8f9',
      '#fae9ee',
      '#f7d9e1',
      '#f3c8d3',
      '#efb4c3',
      '#ea9eb1',
      '#e4829b',
      '#dd5f7e',
      '#d0214d',
      '#830122',
    ],
  },

  zIndex: {
    header: 10,
    overlay: 15,
    drawer: 20,
    headerText: 25,
  },

  breakpoints: ['20rem', '40rem', '50rem', '75rem', '95rem'],
}

export default theme
