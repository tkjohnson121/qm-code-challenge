import styled from '@emotion/styled'

const Button = styled.button`
  cursor: pointer;
  border: none;
  background-color: ${(p) => p.theme.palette.secondary.main};
  color: ${(p) => p.theme.palette.secondary.contrast};
  box-shadow: none;
  border-radius: 2px;
  padding: 0.5rem 2rem;
  line-height: 1;
  max-height: 36px;
  font-weight: 600;
  letter-spacing: 01px;

  &:hover {
  }

  & + button {
    margin: 0 1rem;
  }
`
const PrimaryButton = styled(Button)`
  background-color: ${(p) => p.theme.palette.primary.main};
  color: ${(p) => p.theme.palette.primary.contrast};
`
const SecondaryButton = styled(Button)`
  background-color: ${(p) => p.theme.palette.gray[1]};
  padding: 0.5rem 1rem;
`
const LightButton = styled(Button)``

export { Button, PrimaryButton, SecondaryButton, LightButton }
