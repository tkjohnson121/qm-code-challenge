# Quantum Metric Code Challenge

## Predicate Builder

by [Khari Johnson](https://gvempire.netlify.com)

### What it is:

- A predicate builder that allows users to filter a database table.
- It allows the end user to filter each field with criteria that makes sense to that field (i.e.string and number searches)
- Clicking “Search” generates a query on the server with each of the filters applied

### How to Run:

- clone down the repo

  - `cd qm-code-challenge`

- install client dependencies

  - `cd qm-code-challenge/client`
  - `yarn` or `npm install`

- install server dependencies

  - `cd qm-code-challenge/server`
  - `yarn` or `npm install`

- run both the client application and server application
  - `cd qm-code-challenge/server && yarn start` or `... && npm run start`
  - `cd qm-code-challenge/client && yarn start` or `... && npm run start`

### How it works:

- Styling is done with [**emotion**](https://emotion.sh/docs/introduction), a fast CSS-in-JS library
- Utilizing [**React's Context API**](https://reactjs.org/docs/context.html), each predicate is stored in an array and updated through the context's dispatch function
- After a user inputs the required fields and clicks submit, each field is validated on the **client side**
- A **post** request is then sent to the server at http://localhost:8081/api/predicate/new containing the array of predicates from the App's **usePredicateContext**
- The server parses the array into a SQL query string utilizing switch statements to determine the correct output and `console.log()`s the result as well as returning the string in the response.

### Why this way?:

- [**Emotion**](https://emotion.sh/docs/introduction) is a fast and lightweight and the **ThemeProvider** allows for a lot of reuse in components across differrent domains
- [**React's Context API**](https://reactjs.org/docs/context.html) allows for most of the business logic for the application to be seperate from the UI and view logic. This allows for cleaner and more reusable components to be used elsewhere
- [**Node**](https://nodejs.org/en/) and [**Express**](https://expressjs.com/) because they're my choice server-side language and framework, and they work well with React.

### Challenges Faced:

- **SQL Query**: This project made me realize how much SQL queries have to offer. A couple minutes searching through the documentation gave me everything I needed to know to create the correct query
- **Validation of the Inputs**: At first I wanted each input to be validated seperately `onChange` but found it easier and possibly a better experience to validate when a user tries to submit
- **State Management**: State management is always a rough subject purely because there are so many ways to handle it. Instinctively, I wanted to reach for Redux as the component-level state got larger, but for an application of this size I found it overkill. React's Context API has recently become my favorite method between component-level state and Redux.

### What I would do differently:

- In addition to the client-side validation, I would add server-side validation of the user's input to increse security
- Make the `PredicateContextState` an object instead of an array to rely on object `key` instead of array `index` for adding, removing, and updating predicates

### How I would extend this project:

- Implement authentication and authorization routes and middleware (respectively) to ensure the user can only access their own session
- Connect a [**MariaDB**](https://mariadb.org/) database to the Node/Express server to pull real data instead of `console.log(predicate)`
- Use [**D3**](https://d3js.org/) to display the information from the Node/Express server in a pleasent and easy to digest way

### Coding Challenge Timesheet

- **Project Planning**: 1hr
- **Express**
  - Project Scaffolding: 1hr
  - Implementing the SQL query builder: 2hrs
- **React**
  - Styling: 2hrs
  - Project Scaffolding: 1hr
  - Initializing Components _(App, Predicate Form, Predicate)_: 2hrs
  - State Management _(Predicate Context)_: 2hrs
  - Validation: 2hrs
- **README and Explanation**: 1hr

Time Dashboard: [Wakatime](https://wakatime.com/@tkjohnson121/projects/nllhpcrpmk?start=2019-12-21&end=2019-12-27)
